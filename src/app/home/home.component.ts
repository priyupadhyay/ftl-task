import { Component, OnInit } from '@angular/core';
import {Members} from './member';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Time } from '@angular/common';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  searchedKeyword: string;
  searchStartDate : string;
  searchEndDate :string;
  editProfileForm: FormGroup;
  public id: string;
 public real_name: string;
 public tz:string; 
 public activity_periods:any []=[];
 public start_time: string;
 public end_time :string;
membersList = Members ;


member :any []=[];
constructor(private modalService: NgbModal,private fb: FormBuilder){
  this.editProfileForm = this.fb.group({
    id: [''],
    real_name: [''],
    tz: [''],
    activity_periods: [''],
    start_time: [''],
    end_time: ['']
   });
}

  ngOnInit(): void {
  }
  open(memberModal,member) {
     this.modalService.open(memberModal);
    
     this.editProfileForm.patchValue({
       id: member.id,
      real_name: member.real_name,
      tz: member.tz,
      activity_periods: member.activity_periods,
      
      start_time: member.activity_periods.start_time,
      end_time: member.activity_periods.end_time,
      
     });
  }
  onSubmit() {
    console.log("res:", this.editProfileForm.getRawValue());
    (this.editProfileForm.get("lines") as FormArray)['controls'].splice(0);
    this.modalService.dismissAll();
 
   }
  }